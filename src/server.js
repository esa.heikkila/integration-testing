const express = require('express');
const converter =require('./converter');
const app = express();
const PORT=3000;

app.get('/', (req,res) => res.send("Welcome"));


//RGB to Hex endpoint
app.get('/rgb-to-hex', (req,res) => {
    const red = parseInt(req.query.r, 10);
    const green = parseInt(req.query.g, 10);
    const blue = parseInt(req.query.b,10);
    const hex=converter.rgbToHex(red,green,blue);
    res.send(hex);
});

//Hex to RGP endpoint
app.get('/hexToRpg', (req,res) => {
    var hex = req.query.hex;
    var rgb = converter.hexToRgb(hex);
    res.send(JSON.stringify(rgb));
});

console.log("NODE_ENV: " + process.env.NODE_ENV);
if (process.env.NODE_ENV === 'test') {
    module.exports = app;

} else {
    app.listen(PORT, () => console.log ("Server listening ..."));

}

